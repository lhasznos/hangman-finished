/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



gameState = "not defined state";

$(document).ready(function () {


    $("#guess").click(function () {
        var word = $("#guessedLetterByUser").val();
        $.ajax({
            url: "HangmanServlet",
            type: "POST",
            data: {
                guess: word,
                userName: JSON.parse(gameState).userName
            },
            async: false,
            success: function (result) {
                gameState = result;
            },
            timeout: 3000,
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error");
            }
        });
        var gameStateJSON = JSON.parse(gameState);
        /*reload greeting*/
        $("#greetingToReload").html("Hey " + gameStateJSON.userName + "! Let's play!");
        /*reload proper image*/
        loadProperImg(gameStateJSON);
        /*load current state*/
        loadCurrentState(gameStateJSON);
    });

    $("#submitData").click(function () {
        var userName = $("#userName").val();
        $.ajax({
            url: "GamesManagerServlet",
            type: "POST",
            data: {userName: userName},
            async: false,
            success: function (result) {
                gameState = result;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });
        if (gameState !== "undefined") {
            $("#gameDiv").css("visibility", "visible");
            $("#loginDiv").css("visibility", "hidden");
        }
        var gameStateJSON = JSON.parse(gameState);
        /*reload greeting*/
        $("#greetingToReload").html("Hey " + gameStateJSON.userName + "! Let's play!");
        /*reload proper image*/
        loadProperImg(gameStateJSON);
        /*load current state*/
        loadCurrentState(gameStateJSON);
    });




    function loadProperImg(gameStateJSON) {
        var num = gameStateJSON.failedGuesses;
        if (num > 9)
            num = 9;
        if (num < 0 || num === "null")
            num = 0;
        var imgName = "imgs/hangman" + num + ".png";
        $("#imageToReload").attr("src", imgName);
        $("#imageToReload").attr("alt", imgName);
    }

    function loadCurrentState(gameStateJSON) {
        if (gameStateJSON.guessedSoFar === "null") {
            var length = gameStateJSON.fullWord.length;
            var stringToShow = "_";
            for (i = 0; i < length - 1; i++) {
                stringToShow = stringToShow + "_";
            }
            $("#currentState").html(stringToShow);
        } else {
            $("#currentState").html(gameStateJSON.guessedSoFar);
        }
    }

});


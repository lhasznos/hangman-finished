/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hasznosl.hangman;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author laszlo
 */
public class ProvideOngoingGamesServlet extends HttpServlet {

    private static SessionFactory createSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();
        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

    private static StringBuilder addJSONPropertyValuePair(StringBuilder sb, String propertyName, String propertyValue, boolean isLast) {
        StringBuilder ret = sb.append("\"").append(propertyName).append("\"").append(":").append("\"").append(propertyValue).append("\"");
        if (isLast) {
            /*no coma needed*/
        } else {
            ret = sb.append(", ");
        }
        return ret;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String onlyOngoing = request.getParameter("onlyOngoing");
        if (onlyOngoing != null) {
            SessionFactory sessionFactory = createSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Game.class);
            if (onlyOngoing.equals("true")) {
                criteria.add(Restrictions.eq("gameFinished", false));
            }

            List<Game> games = (List<Game>) criteria.list();
            session.getTransaction().commit();
            session = sessionFactory.openSession();
            session.beginTransaction();
            StringBuilder jsonText = new StringBuilder();
            jsonText.append("{\"games\":[");
            for (Game game : games) {
                jsonText.append("{");
                addJSONPropertyValuePair(jsonText, "userName", game.getUserName(), false);
                addJSONPropertyValuePair(jsonText, "fullWord", game.getFullWord(), false);
                addJSONPropertyValuePair(jsonText, "guessedSoFar", game.getGuessedSoFar(), false);
                addJSONPropertyValuePair(jsonText, "gameFinished", game.isGameFinished() ? "true" : "false", false);
                addJSONPropertyValuePair(jsonText, "failedGuesses", Integer.valueOf(game.getFailedGuesses()).toString(), true);
                jsonText.append("},");
                
            }
            jsonText.replace(jsonText.lastIndexOf(","), jsonText.lastIndexOf(",")+1, "");
            jsonText.append("]}");
            String jsonString = jsonText.toString();
            response.getWriter().write(jsonString);
        } else {
            response.getWriter().write("Null provided");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

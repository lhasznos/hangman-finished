/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hasznosl.hangman;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringTokenizer;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author laszlo
 */
@Entity
public class Game implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int failedGuesses = 0;
    private String userName;
    private String fullWord;
    private String guessedSoFar;
    private boolean gameFinished = false;

    public int getFailedGuesses() {
        return failedGuesses;
    }

    public void setFailedGuesses(int failedGuesses) {
        this.failedGuesses = failedGuesses;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + this.failedGuesses;
        hash = 23 * hash + Objects.hashCode(this.userName);
        hash = 23 * hash + Objects.hashCode(this.fullWord);
        hash = 23 * hash + Objects.hashCode(this.guessedSoFar);
        hash = 23 * hash + (this.gameFinished ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        return true;
    }

    public Game(){
        
    }
    
    public Game(String userName, String fullWord){
        this.userName = userName;
        this.fullWord = fullWord;
        
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isGameFinished() {
        return gameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        this.gameFinished = gameFinished;
    }

    public String getFullWord() {
        return fullWord;
    }

    public String getGuessedSoFar() {
        return guessedSoFar;
    }

    public void setGuessedSoFar(String guessedSoFar) {
        this.guessedSoFar = guessedSoFar;
    }
    

    @Override
    public String toString() {
        return "com.hasznosl.hangman.Game[ id=" + id + " ]";
    }
    
    
    public boolean makeAGuess(String guessString){
        if(guessString == null){
            this.failedGuesses++;
            if(failedGuesses > 8){
                this.gameFinished = true;
            }
            return false;
        }
        if(guessString.length() > 1){
            /*treat it as a word*/
            if(guessString.equals(this.fullWord)){
                this.gameFinished = true;
                return true;
            } else {
                this.failedGuesses++;
                if(failedGuesses > 8){
                    this.gameFinished = true;
                }
                return false;
            }
        } else if(guessString.length() == 1) {
            /*treat it as a letter*/
            if(this.fullWord.contains(guessString)){
                int i = 0;
                
                if((this.guessedSoFar == null) || (this.guessedSoFar.length() == 0)){
                    StringBuilder sb = new StringBuilder();
                    while(i<this.fullWord.length()){
                        if(this.fullWord.charAt(i) == guessString.charAt(0)){
                            sb.append(this.fullWord.charAt(i));
                        } else {
                            sb.append("_");
                        }
                        
                        i++;
                    }    
                    this.guessedSoFar = sb.toString();
                } else {
                    StringBuilder sb = new StringBuilder();
                    while(i<this.fullWord.length()){
                        if(this.fullWord.charAt(i) == guessString.charAt(0)){
                            sb.append(this.fullWord.charAt(i));
                        } else if(this.guessedSoFar.charAt(i) == '_') {
                            sb.append("_");
                        } else {
                            sb.append(this.guessedSoFar.charAt(i));
                        }
                        i++;
                    } 
                    this.guessedSoFar = sb.toString();
                }
                
                if(this.guessedSoFar.equals(this.fullWord)){
                    this.gameFinished = true;
                }
                
                return true;
            } else {
                this.failedGuesses++;
                if(failedGuesses > 8){
                    this.gameFinished = true;
                }
                return false;
            }
        } else if(guessString.length() < 1){
            this.failedGuesses++;
            return false;
        }
        return false;
    }
    
}

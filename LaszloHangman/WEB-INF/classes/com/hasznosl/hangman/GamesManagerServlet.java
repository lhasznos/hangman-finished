/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hasznosl.hangman;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author laszlo
 */
public class GamesManagerServlet extends HttpServlet {

    
    static List<String> possibleWords = new ArrayList<>();
    
    private static SessionFactory createSessionFactory(){
	Configuration configuration = new Configuration();
	configuration.configure();
	StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
	SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	return sessionFactory;
    }
    private static StringBuilder addJSONPropertyValuePair(StringBuilder sb, String propertyName, String propertyValue, boolean isLast){
        StringBuilder ret = sb.append("\"").append(propertyName).append("\"").append(":").append("\"").append(propertyValue).append("\"");
        if(isLast){
            /*no coma needed*/
        } else {
            ret = sb.append(", ");
        }
        return ret;
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userName = request.getParameter("userName");
        
        if (userName != null) {
            try {

                SessionFactory sessionFactory = createSessionFactory();
                Session session = sessionFactory.openSession();
                session.beginTransaction();

                Criteria criteria = session.createCriteria(Game.class);
                criteria.add(Restrictions.eq("userName", userName));
                criteria.add(Restrictions.eq("gameFinished", false));

                List<Game> gamesOfUser = (List<Game>)criteria.list();
                session.getTransaction().commit();
                session = sessionFactory.openSession();
                session.beginTransaction();
                if(gamesOfUser.size() < 1){
                    /*In the front end JQuery will set the content so a new game is started from the beginning*/
                    int rand = (int)(Math.random()*7);/*7 possible challenges in the pool currently*/
                    String fullWord = possibleWords.get(rand);  
                    Game newGame = new Game(userName, fullWord);
                    session.save(newGame);
                    session.getTransaction().commit();
                    StringBuilder jsonText = new StringBuilder();
                    jsonText.append("{");
                    addJSONPropertyValuePair(jsonText, "userName", newGame.getUserName(), false);
                    addJSONPropertyValuePair(jsonText, "fullWord", newGame.getFullWord(), false);
                    addJSONPropertyValuePair(jsonText, "guessedSoFar", newGame.getGuessedSoFar(), false);
                    addJSONPropertyValuePair(jsonText, "gameFinished", newGame.isGameFinished()?"true":"false", false);
                    addJSONPropertyValuePair(jsonText, "failedGuesses", Integer.valueOf(newGame.getFailedGuesses()).toString(),true);
                    jsonText.append("}");
                    String jsonString = jsonText.toString();
                    response.getWriter().write(jsonString);
                } else if(gamesOfUser.size() > 1){
                    response.getWriter().write("Should not happen. There should be no more then one left game by user " + userName);
                    /*TODO: shuold not happen, do not handle it for this simple example*/
                } else {
                    /*In the front end JQuery will set the content so left game can be continued*/
                    Game game = gamesOfUser.get(0);
                    StringBuilder jsonText = new StringBuilder();
                    jsonText.append("{");
                    addJSONPropertyValuePair(jsonText, "userName", game.getUserName(), false);
                    addJSONPropertyValuePair(jsonText, "fullWord", game.getFullWord(), false);
                    addJSONPropertyValuePair(jsonText, "guessedSoFar", game.getGuessedSoFar(), false);
                    addJSONPropertyValuePair(jsonText, "gameFinished", game.isGameFinished()?"true":"false", false);
                    addJSONPropertyValuePair(jsonText, "failedGuesses", Integer.valueOf(game.getFailedGuesses()).toString(),true);
                    jsonText.append("}");
                    String jsonString = jsonText.toString();
                    response.getWriter().write(jsonString);
                }
                
            }catch (HibernateException ex){
                response.getWriter().write("Hibernate: " + ex.getMessage());
            } catch (Exception ex) {
                response.getWriter().write("Exception in Servlet: " + ex.getMessage());
            }
        } else {
            response.getWriter().write("Servlet: No name printed.");
            /*TODO: not handled yet*/
        }
            

        
    }

    @Override
    public void init() throws ServletException {
        /*very basic, but no time for more sophisticated pool of challenges*/
        possibleWords.add("HTML");
        possibleWords.add("Hibernate");
        possibleWords.add("Ajax");
        possibleWords.add("JQuery");
        possibleWords.add("Javascript");
        possibleWords.add("Java");
        possibleWords.add("Ajax");
    }



    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

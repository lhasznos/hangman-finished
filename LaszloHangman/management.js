/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




$(document).ready(function () {
    setInterval(refreshOngoingGames, 3000);
});

function refreshOngoingGames() {
    var resultJSON;
    $.ajax({
        url: "ProvideOngoingGamesServlet",
        type: "POST",
        data: {onlyOngoing: "true"},
        async: false,
        success: function (result) {
            $("#updateGames").css("visibility", "hidden");
            resultJSON = JSON.parse(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        },
        timeout: 3000
    });

    var games = resultJSON.games;
    var i = 0;
    var htmlString = '<table style="width:100%"><tr><th><b>User Name</b></th><th><b>Guessed So Far</b></th><th><b>Challenge</b></th><th><b>Failed Guesses</b></th><th><b>Game Finished?</b></th></tr>';

    for (; i < games.length; i++) {
        htmlString = htmlString +
                "<tr>" +
                "<td>" + games[i].userName + "</td>" +
                "<td>" + games[i].guessedSoFar + "</td>" +
                "<td>" + games[i].fullWord + "</td> " +
                "<td>" + games[i].failedGuesses + "</td>" +
                "<td>" + games[i].gameFinished + "</td>"
                "</tr>";
    }
    htmlString = htmlString + "</table>";
    $("#ongoingGames").html(htmlString);
}